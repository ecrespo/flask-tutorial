#!/usr/bin/env python

from flask import Flask,jsonify,json


app = Flask(__name__)

lista = ["John Doe","Jane Doe"]

@app.route("/ListaEmpleados")
def ListaEmpleados():
    
    try:
    	#inicializar la lista de empleados
    	listaEmpleados = []

    	#crear instancias para llenar la lista
    	for i  in lista:
    		datos = i.split(" ")
    		listaEmpleados.append({'nombre': datos[0],'apellido': datos[1]})    
        # convertir en dato json al diccionario
        jsonStr = json.dumps(listaEmpleados)

    except Exception ,e:
        print str(e)

    #Retorna el json

    return jsonify(Empleados=jsonStr)


if __name__ == '__main__':
	app.run()